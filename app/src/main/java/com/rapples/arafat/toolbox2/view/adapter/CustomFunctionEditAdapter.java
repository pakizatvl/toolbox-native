package com.rapples.arafat.toolbox2.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rapples.arafat.toolbox2.Database.Acquisition_DB;
import com.rapples.arafat.toolbox2.Database.CustomFunction_DB;
import com.rapples.arafat.toolbox2.Database.MasterExecutor;
import com.rapples.arafat.toolbox2.R;
import com.rapples.arafat.toolbox2.databinding.ModelCustomFunctionEditProductDesignBinding;
import com.rapples.arafat.toolbox2.model.CustomFunctionProduct;
import com.rapples.arafat.toolbox2.model.Field;
import com.rapples.arafat.toolbox2.model.Product;
import com.rapples.arafat.toolbox2.util.SharedPref;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class CustomFunctionEditAdapter extends RecyclerView.Adapter<CustomFunctionEditAdapter.ViewHolder> {

    List<CustomFunctionProduct> customFunctionProductList;
    Context context;
    String fileName;
    private SharedPreferences sharedPreferences;
    private List<Field> fieldList = new ArrayList<>();
    private String fieldListInString;
    private boolean quantity;

    public CustomFunctionEditAdapter(List<CustomFunctionProduct> customFunctionProductList, Context context, String fileName) {
        this.customFunctionProductList = customFunctionProductList;
        this.context = context;
        this.fileName = fileName;
        sharedPreferences = context.getSharedPreferences(SharedPref.SETTING_PREFERENCE, Context.MODE_PRIVATE);
        fieldListInString = sharedPreferences.getString(SharedPref.CUSTOM_FIELD_LIST, "");
        if (!fieldListInString.isEmpty()) {
            Gson gson = new Gson();
            Type type = new TypeToken<List<Field>>() {
            }.getType();
            fieldList = gson.fromJson(fieldListInString, type);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ModelCustomFunctionEditProductDesignBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.model_custom_function_edit_product_design, parent, false);
        return new ViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        quantity = sharedPreferences.getBoolean(SharedPref.QUANTITY_FIELD, false);

        final CustomFunctionProduct customFunctionProduct = customFunctionProductList.get(position);

        holder.binding.field1Ll.setVisibility(View.VISIBLE);
        holder.binding.field1titleTv.setText(fieldList.get(0).getFieldName());

        if (quantity && fieldList.get(0).getFieldName().toLowerCase().equals("quantity")) {

            holder.binding.field1Value.setVisibility(View.GONE);
            holder.binding.field1EditLL.setVisibility(View.VISIBLE);
            holder.binding.field1EditValue.setText(customFunctionProduct.getField1());

        } else {
            holder.binding.field1Value.setText(customFunctionProduct.getField1());

        }

        if (fieldList.size() > 1) {
            holder.binding.field2Ll.setVisibility(View.VISIBLE);
            holder.binding.field2titleTv.setText(fieldList.get(1).getFieldName());

            if (quantity && fieldList.get(1).getFieldName().toLowerCase().equals("quantity")) {

                holder.binding.field2Value.setVisibility(View.GONE);
                holder.binding.field2EditLL.setVisibility(View.VISIBLE);
                holder.binding.field2EditValue.setText(customFunctionProduct.getField2());
            } else {
                holder.binding.field2Value.setText(customFunctionProduct.getField2());

            }
        }

        if (fieldList.size() > 2) {
            holder.binding.field3Ll.setVisibility(View.VISIBLE);
            holder.binding.field3titleTv.setText(fieldList.get(2).getFieldName());

            if (quantity && fieldList.get(2).getFieldName().toLowerCase().equals("quantity")) {

                holder.binding.field3Value.setVisibility(View.GONE);
                holder.binding.field3EditLL.setVisibility(View.VISIBLE);
                holder.binding.field3EditValue.setText(customFunctionProduct.getField3());

            } else {
                holder.binding.field3Value.setText(customFunctionProduct.getField3());

            }
        }
        if (fieldList.size() > 3) {
            holder.binding.field4Ll.setVisibility(View.VISIBLE);
            holder.binding.field4titleTv.setText(fieldList.get(3).getFieldName());

            if (quantity && fieldList.get(3).getFieldName().toLowerCase().equals("quantity")) {

                holder.binding.field4Value.setVisibility(View.GONE);
                holder.binding.field4EditLL.setVisibility(View.VISIBLE);
                holder.binding.field4EditValue.setText(customFunctionProduct.getField4());

            } else {
                holder.binding.field4Value.setText(customFunctionProduct.getField4());
            }
        }
        if (fieldList.size() > 4) {
            holder.binding.field5Ll.setVisibility(View.VISIBLE);
            holder.binding.field5titleTv.setText(fieldList.get(4).getFieldName());

            if (quantity && fieldList.get(4).getFieldName().toLowerCase().equals("quantity")) {

                holder.binding.field5Value.setVisibility(View.GONE);
                holder.binding.field5EditLL.setVisibility(View.VISIBLE);
                holder.binding.field5EditValue.setText(customFunctionProduct.getField5());

            } else {
                holder.binding.field5Value.setText(customFunctionProduct.getField5());
            }
        }


        holder.binding.incrementRl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field1EditValue.setText(Integer.parseInt(holder.binding.field1EditValue.getText().toString()) + 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        Integer.parseInt(holder.binding.field1EditValue.getText().toString()) + "",
                        customFunctionProduct.getField2(),
                        customFunctionProduct.getField3(),
                        customFunctionProduct.getField4(),
                        customFunctionProduct.getField5()

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });

        holder.binding.decrementRL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field1EditValue.setText(Integer.parseInt(holder.binding.field1EditValue.getText().toString()) - 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        Integer.parseInt(holder.binding.field1EditValue.getText().toString())+"",
                        customFunctionProduct.getField2(),
                        customFunctionProduct.getField3(),
                        customFunctionProduct.getField4(),
                        customFunctionProduct.getField5()

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });


        holder.binding.incrementRl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field2EditValue.setText(Integer.parseInt(holder.binding.field2EditValue.getText().toString()) + 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        customFunctionProduct.getField1(),
                        Integer.parseInt(holder.binding.field2EditValue.getText().toString())+ "",
                        customFunctionProduct.getField3(),
                        customFunctionProduct.getField4(),
                        customFunctionProduct.getField5()

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });

        holder.binding.decrementRL2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field2EditValue.setText(Integer.parseInt(holder.binding.field2EditValue.getText().toString()) - 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        customFunctionProduct.getField1(),
                        Integer.parseInt(holder.binding.field2EditValue.getText().toString())+ "",
                        customFunctionProduct.getField3(),
                        customFunctionProduct.getField4(),
                        customFunctionProduct.getField5()

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });

        holder.binding.incrementRl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field3EditValue.setText(Integer.parseInt(holder.binding.field3EditValue.getText().toString()) + 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        customFunctionProduct.getField1(),
                        customFunctionProduct.getField2(),
                        Integer.parseInt(holder.binding.field3EditValue.getText().toString())+ "",
                        customFunctionProduct.getField4(),
                        customFunctionProduct.getField5()

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });

        holder.binding.decrementRL3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field3EditValue.setText(Integer.parseInt(holder.binding.field3EditValue.getText().toString()) - 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        customFunctionProduct.getField1(),
                        customFunctionProduct.getField2(),
                        Integer.parseInt(holder.binding.field3EditValue.getText().toString())+ "",
                        customFunctionProduct.getField4(),
                        customFunctionProduct.getField5()

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });

        holder.binding.incrementRl4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field4EditValue.setText(Integer.parseInt(holder.binding.field4EditValue.getText().toString()) + 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        customFunctionProduct.getField1(),
                        customFunctionProduct.getField2(),
                        customFunctionProduct.getField3(),
                        Integer.parseInt(holder.binding.field4EditValue.getText().toString())+ "",
                        customFunctionProduct.getField5()

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });

        holder.binding.decrementRL4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field4EditValue.setText(Integer.parseInt(holder.binding.field4EditValue.getText().toString()) - 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        customFunctionProduct.getField1(),
                        customFunctionProduct.getField2(),
                        customFunctionProduct.getField3(),
                        Integer.parseInt(holder.binding.field4EditValue.getText().toString()) + "",
                        customFunctionProduct.getField5()

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });

        holder.binding.incrementRl5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field5EditValue.setText(Integer.parseInt(holder.binding.field5EditValue.getText().toString()) + 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        customFunctionProduct.getField1(),
                        customFunctionProduct.getField2(),
                        customFunctionProduct.getField3(),
                        customFunctionProduct.getField4(),
                        Integer.parseInt(holder.binding.field5EditValue.getText().toString()) + ""

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });

        holder.binding.decrementRL5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.binding.field5EditValue.setText(Integer.parseInt(holder.binding.field5EditValue.getText().toString()) - 1 + "");

                final CustomFunctionProduct item = new CustomFunctionProduct(
                        customFunctionProduct.getId(),
                        customFunctionProduct.getFileId(),
                        customFunctionProduct.getField1(),
                        customFunctionProduct.getField2(),
                        customFunctionProduct.getField3(),
                        customFunctionProduct.getField4(),
                        Integer.parseInt(holder.binding.field5EditValue.getText().toString())+ ""

                );

                MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        CustomFunction_DB.getInstance(context.getApplicationContext()).CustomFunctionProductDao().updateProduct(item);
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
            }
        });


    }

    @Override
    public int getItemCount() {
        return customFunctionProductList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ModelCustomFunctionEditProductDesignBinding binding;

        public ViewHolder(ModelCustomFunctionEditProductDesignBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
