package com.rapples.arafat.toolbox2.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rapples.arafat.toolbox2.OnFieldChangeListener;
import com.rapples.arafat.toolbox2.R;
import com.rapples.arafat.toolbox2.model.Field;
import com.rapples.arafat.toolbox2.view.activity.ApplicationSettingsActivity;
import com.rapples.arafat.toolbox2.view.activity.BarcodeComparisonActivity;

import java.util.ArrayList;
import java.util.List;

public class CustomFieldAdapter extends RecyclerView.Adapter<CustomFieldAdapter.ViewHolder> {

    List<Field> fieldList;
    List<Field> fieldDataList = new ArrayList<>();
    Context context;
    ArrayAdapter<String> adapter;
    OnFieldChangeListener onFieldChangeListener;

    public CustomFieldAdapter(List<Field> fieldList, Context context, OnFieldChangeListener onFieldChangeListener) {
        this.fieldList = fieldList;
        this.context = context;
        this.onFieldChangeListener = onFieldChangeListener;
        String[] items = new String[]{"Barcode", "Numeric", "TextField"};
        adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, items);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_field_design, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        Field field = fieldList.get(position);
        fieldDataList.add(field);

        holder.fieldName.setText(field.getFieldName());
        holder.fieldType.setAdapter(adapter);

//
//        if (field.getFieldType() != null) {
//            if (field.getFieldType().equals("Barcode")) {
//                holder.fieldType.setSelection(0);
//            } else if (field.getFieldType().equals("Numeric")) {
//                holder.fieldType.setSelection(1);
//            } else if (field.getFieldType().equals("TextField")) {
//                holder.fieldType.setSelection(2);
//            }
//        }


        holder.fieldCount.setText("Field " + String.valueOf(position + 1));
        holder.fieldName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    fieldDataList.get(position).setFieldName(holder.fieldName.getText().toString());
                    fieldDataList.get(position).setFieldType(holder.fieldType.getSelectedItem().toString());

                    onFieldChangeListener.updateFieldList(fieldDataList);
                    disableFocus();
                    hideKeyboard((Activity) context);
                    handled = true;
                }
                return handled;
            }
        });



        if (position == fieldDataList.size() - 1) {
            holder.fieldName.requestFocus();
        }


       // holder.setIsRecyclable(false);


    }

    @Override
    public int getItemCount() {
        return fieldList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private Spinner fieldType;
        private EditText fieldName;
        private TextView fieldCount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fieldName = itemView.findViewById(R.id.fieldNameEt);
            fieldType = itemView.findViewById(R.id.fieldTypeDropdown);
            fieldCount = itemView.findViewById(R.id.fieldCountTv);

        }
    }

    private void disableFocus() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
