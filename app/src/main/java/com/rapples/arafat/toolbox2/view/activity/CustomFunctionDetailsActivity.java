package com.rapples.arafat.toolbox2.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.rapples.arafat.toolbox2.Database.Acquisition_DB;
import com.rapples.arafat.toolbox2.Database.CustomFunction_DB;
import com.rapples.arafat.toolbox2.Database.MasterExecutor;
import com.rapples.arafat.toolbox2.R;
import com.rapples.arafat.toolbox2.databinding.ActivityCustomFunctionDetailsBinding;
import com.rapples.arafat.toolbox2.model.CustomFunction;
import com.rapples.arafat.toolbox2.model.CustomFunctionProduct;
import com.rapples.arafat.toolbox2.model.Product;
import com.rapples.arafat.toolbox2.util.SharedPref;
import com.rapples.arafat.toolbox2.view.adapter.CustomDataAcquisitionAdapter;
import com.rapples.arafat.toolbox2.view.adapter.CustomDataAcquisitionEditAdapter;
import com.rapples.arafat.toolbox2.view.adapter.CustomFunctionEditAdapter;
import com.rapples.arafat.toolbox2.view.adapter.CustomFunctionProductAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class CustomFunctionDetailsActivity extends AppCompatActivity {

    private ActivityCustomFunctionDetailsBinding binding;
    private String fileName;
    private String date;
    private int id;
    private String editable;
    private List<CustomFunctionProduct> customFunctionProductList;
    private CustomFunctionProductAdapter adapter;
    private CustomFunctionEditAdapter editAdapter;
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_custom_function_details);

        init();

        getIntentData();

        getProductList();
    }

    @Override
    protected void onResume() {
        getProductList();
        super.onResume();
    }

    private void getIntentData() {
        fileName = getIntent().getStringExtra(SharedPref.FILE_NAME);
        date = getIntent().getStringExtra(SharedPref.DATE);
        id = Integer.parseInt(getIntent().getStringExtra(SharedPref.ID));
        editable = getIntent().getStringExtra(SharedPref.EDITABLE);


        binding.fileNameTv.setText(fileName);
        binding.dateTv.setText(date);

    }

    private void init() {
        customFunctionProductList = new ArrayList<>();
        sharedPreferences = getSharedPreferences(SharedPref.SETTING_PREFERENCE,MODE_PRIVATE);
        binding.toolBarTitleTv.setText(sharedPreferences.getString(SharedPref.CUSTOM_FUNCTION_NAME,""));

    }

    private void getProductList() {

        MasterExecutor.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                customFunctionProductList = CustomFunction_DB.getInstance(CustomFunctionDetailsActivity.this).CustomFunctionProductDao().loadAllproduct(fileName);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.valueCountTv.setText(customFunctionProductList.size() + " Values");
                        for (int i = 0; i < customFunctionProductList.size(); i++) {
                        }
                        if (customFunctionProductList.size() > 0) {
                            Collections.reverse(customFunctionProductList);
                        }
                        binding.customFunctionalityRecyclerview.setLayoutManager(new LinearLayoutManager(CustomFunctionDetailsActivity.this));

                        adapter = new CustomFunctionProductAdapter(customFunctionProductList, CustomFunctionDetailsActivity.this);
                        binding.customFunctionalityRecyclerview.setAdapter(adapter);


                        if (editable.equals("true")) {
                            editAdapter = new CustomFunctionEditAdapter(customFunctionProductList, CustomFunctionDetailsActivity.this,fileName);
                            binding.customFunctionalityRecyclerview.setAdapter(editAdapter);

                        } else {
                            adapter = new CustomFunctionProductAdapter(customFunctionProductList, CustomFunctionDetailsActivity.this);
                            binding.customFunctionalityRecyclerview.setAdapter(adapter);

                        }


                        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
                        itemTouchHelper.attachToRecyclerView(binding.customFunctionalityRecyclerview);


                    }
                });
            }
        });

    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return true;
        }


        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            final int position = viewHolder.getAdapterPosition();

            switch (direction) {
                case ItemTouchHelper.RIGHT:

                    break;

                case ItemTouchHelper.LEFT:
                    deleteItem(position);
                    Toast.makeText(CustomFunctionDetailsActivity.this, "File deleted", Toast.LENGTH_SHORT).show();
                    break;
            }

        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            new RecyclerViewSwipeDecorator.Builder(CustomFunctionDetailsActivity.this, c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addSwipeRightBackgroundColor(ContextCompat.getColor(CustomFunctionDetailsActivity.this, R.color.green))
                    .addSwipeRightActionIcon(R.drawable.ic_baseline_edit_24)
                    .addSwipeLeftBackgroundColor(ContextCompat.getColor(CustomFunctionDetailsActivity.this, R.color.red))
                    .addSwipeLeftActionIcon(R.drawable.ic_baseline_delete_24)
                    .setActionIconTint(ContextCompat.getColor(recyclerView.getContext(), android.R.color.white))
                    .create()
                    .decorate();
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };

    private void deleteItem(final int position) {
        MasterExecutor.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                CustomFunction_DB.getInstance(CustomFunctionDetailsActivity.this).CustomFunctionProductDao().deleteProduct(customFunctionProductList.get(position));
                customFunctionProductList.remove(customFunctionProductList.get(position));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getProductList();
                    }
                });
            }
        });
    }


    public void onBackCustomFunctionDetails(View view) {
        onBackPressed();
    }

    public void onSettingsFromCustomFunctionDetails(View view) {
    }

    public void deleteFile(View view) {
        final CustomFunction customFunction = new CustomFunction(id,fileName,date);
        deleteProduct(fileName);
        MasterExecutor.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                CustomFunction_DB.getInstance(getApplicationContext()).CustomFunctionDao().deleteCustomFunctionData(customFunction);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                });
            }
        });
    }

    private void deleteProduct(final String fileName) {

        MasterExecutor.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                customFunctionProductList = CustomFunction_DB.getInstance(CustomFunctionDetailsActivity.this).CustomFunctionProductDao().loadAllproduct(fileName);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MasterExecutor.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i < customFunctionProductList.size(); i++) {
                                    CustomFunction_DB.getInstance(CustomFunctionDetailsActivity.this).CustomFunctionProductDao().deleteProduct(customFunctionProductList.get(i));
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    });
                                }
                            }
                        });


                    }
                });
            }
        });
    }

    public void addNewElement(View view) {
        startActivity(new Intent(CustomFunctionDetailsActivity.this, AddCustomFunctionDataAcquisition.class)
                .putExtra(SharedPref.FILE_NAME, fileName)
                .putExtra(SharedPref.IS_ADDED, true));
    }
}