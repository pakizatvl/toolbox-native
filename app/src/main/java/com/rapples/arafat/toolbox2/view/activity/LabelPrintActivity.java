package com.rapples.arafat.toolbox2.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.rapples.arafat.toolbox2.Database.MasterData_DB;
import com.rapples.arafat.toolbox2.Database.MasterExecutor;
import com.rapples.arafat.toolbox2.R;
import com.rapples.arafat.toolbox2.databinding.ActivityLabelPrintBinding;
import com.rapples.arafat.toolbox2.model.Masterdata;
import com.rapples.arafat.toolbox2.util.DOPrintSettings;
import com.rapples.arafat.toolbox2.util.SharedPref;
import com.rapples.arafat.toolbox2.view.adapter.CustomMasterDataAdapter;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import honeywell.connection.ConnectionBase;
import honeywell.connection.Connection_Bluetooth;
import honeywell.connection.Connection_TCP;
import honeywell.printer.DocumentDPL;
import honeywell.printer.DocumentEZ;
import honeywell.printer.DocumentExPCL_LP;
import honeywell.printer.DocumentExPCL_PP;
import honeywell.printer.DocumentLP;
import honeywell.printer.ParametersDPL;
import honeywell.printer.ParametersEZ;
import honeywell.printer.ParametersExPCL_LP;
import honeywell.printer.ParametersExPCL_PP;
import honeywell.printer.UPSMessage;
import honeywell.printer.configuration.dpl.AutoUpdate_DPL;
import honeywell.printer.configuration.dpl.AvalancheEnabler_DPL;
import honeywell.printer.configuration.dpl.BluetoothConfiguration_DPL;
import honeywell.printer.configuration.dpl.Fonts_DPL;
import honeywell.printer.configuration.dpl.MediaLabel_DPL;
import honeywell.printer.configuration.dpl.MemoryModules_DPL;
import honeywell.printer.configuration.dpl.Miscellaneous_DPL;
import honeywell.printer.configuration.dpl.NetworkGeneralSettings_DPL;
import honeywell.printer.configuration.dpl.NetworkWirelessSettings_DPL;
import honeywell.printer.configuration.dpl.PrintSettings_DPL;
import honeywell.printer.configuration.dpl.PrinterInformation_DPL;
import honeywell.printer.configuration.dpl.PrinterStatus_DPL;
import honeywell.printer.configuration.dpl.SensorCalibration_DPL;
import honeywell.printer.configuration.dpl.SerialPortConfiguration_DPL;
import honeywell.printer.configuration.dpl.SystemSettings_DPL;
import honeywell.printer.configuration.expcl.BatteryCondition_ExPCL;
import honeywell.printer.configuration.expcl.BluetoothConfiguration_ExPCL;
import honeywell.printer.configuration.expcl.GeneralStatus_ExPCL;
import honeywell.printer.configuration.expcl.MagneticCardData_ExPCL;
import honeywell.printer.configuration.expcl.MemoryStatus_ExPCL;
import honeywell.printer.configuration.expcl.PrinterOptions_ExPCL;
import honeywell.printer.configuration.expcl.PrintheadStatus_ExPCL;
import honeywell.printer.configuration.expcl.VersionInformation_ExPCL;
import honeywell.printer.configuration.ez.AvalancheSettings;
import honeywell.printer.configuration.ez.BatteryCondition;
import honeywell.printer.configuration.ez.BluetoothConfiguration;
import honeywell.printer.configuration.ez.FontData;
import honeywell.printer.configuration.ez.FontList;
import honeywell.printer.configuration.ez.FormatData;
import honeywell.printer.configuration.ez.FormatList;
import honeywell.printer.configuration.ez.GeneralConfiguration;
import honeywell.printer.configuration.ez.GeneralStatus;
import honeywell.printer.configuration.ez.GraphicData;
import honeywell.printer.configuration.ez.GraphicList;
import honeywell.printer.configuration.ez.IrDAConfiguration;
import honeywell.printer.configuration.ez.LabelConfiguration;
import honeywell.printer.configuration.ez.MagneticCardConfiguration;
import honeywell.printer.configuration.ez.MagneticCardData;
import honeywell.printer.configuration.ez.ManufacturingDate;
import honeywell.printer.configuration.ez.MemoryStatus;
import honeywell.printer.configuration.ez.PrinterOptions;
import honeywell.printer.configuration.ez.PrintheadStatus;
import honeywell.printer.configuration.ez.SerialNumber;
import honeywell.printer.configuration.ez.SmartCardConfiguration;
import honeywell.printer.configuration.ez.TCPIPStatus;
import honeywell.printer.configuration.ez.UpgradeData;
import honeywell.printer.configuration.ez.VersionInformation;

public class LabelPrintActivity extends AppCompatActivity implements Runnable {
    private static final String ACTION_BARCODE_DATA = "com.honeywell.sample.action.BARCODE_DATA";
    private static final String ACTION_CLAIM_SCANNER = "com.honeywell.aidc.action.ACTION_CLAIM_SCANNER";
    private static final String ACTION_RELEASE_SCANNER = "com.honeywell.aidc.action.ACTION_RELEASE_SCANNER";
    private static final String EXTRA_SCANNER = "com.honeywell.aidc.extra.EXTRA_SCANNER";
    private static final String EXTRA_PROFILE = "com.honeywell.aidc.extra.EXTRA_PROFILE";
    private static final String EXTRA_PROPERTIES = "com.honeywell.aidc.extra.EXTRA_PROPERTIES";

    //Keys to pass data to/from FileBrowseActivity
    static final String FOLDER_NAME_KEY = "com.honeywell.doprint.Folder_Name_Key";
    static final String FOLDER_PATH_KEY = "com.honeywell.doprint.Folder_Path_Key";

    //Keys to pass data to Connection Activity
    static final String CONNECTION_MODE_KEY = "com.honeywell.doprint.Connection_Mode_Key";
    static final String PRINTER_IPADDRESS_KEY = "com.honeywell.doprint.PRINTER_IPAddress_Key";
    static final String PRINTER_TCPIPPORT_KEY = "com.honeywell.doprint.PRINTER_TCPIPPort_Key";
    static final String BLUETOOTH_DEVICE_NAME_KEY = "com.honeywell.doprint.PRINTER_Bluetooth_Device_Name_Key";
    static final String BLUETOOTH_DEVICE_ADDR_KEY = "com.honeywell.doprint.PRINTER_Bluetooth_Device_Addr_Key";

    //Variable for folder content
    private String m_selectedPath;

    //Variable for Connection information
    private String m_printerIP = null;
    private String m_printerMAC = null;
    private int m_printerPort = 515;
    private String connectionType;
    ConnectionBase conn = null;
    private int m_printHeadWidth = 384;

    //Variable for connection status
    private String g_PrintStatusStr;

    ArrayAdapter<CharSequence> adapter = null;

    //array to contain the filenames inside a directory
    List<String> filesList = new ArrayList<>();

    static final int CONFIG_CONNECTION_REQUEST = 0; // for Connection Settings
    private static final int REQUEST_PICK_FILE = 1; //for File browsing

    //Document and Parameter Objects
    private DocumentEZ docEZ;
    private DocumentLP docLP;
    private DocumentDPL docDPL;
    private DocumentExPCL_LP docExPCL_LP;
    private DocumentExPCL_PP docExPCL_PP;

    private ParametersEZ paramEZ;
    private ParametersDPL paramDPL;
    private ParametersExPCL_LP paramExPCL_LP;
    private ParametersExPCL_PP paramExPCL_PP;
    // use to update the UI information.
    private Handler m_handler = new Handler(); // Main thread

    private String m_printerMode = "";
    private int selectedItemIndex = 0;

    //====UI Controls========//
    //Buttons
    Button m_browseButton;
    Button m_printButton;
    Button m_saveButton;
    Button m_configConnectionButton;
    RadioGroup m_performTaskRadioGroup;
    RadioButton m_printRadioButton;
    RadioButton m_queryRadioButton;

    //EditText
    TextView m_connectionInfoStatus;
    TextView m_actionTextView;

    //Spinners
    Spinner m_connectionSpinner;
    Spinner m_printerModeSpinner;
    Spinner m_printItemsSpinner;
    Spinner m_printHeadSpinner;

    byte[] printData = {0};

    // Configuration files to load
    String ApplicationConfigFilename = "applicationconfig.dat";

    DOPrintSettings g_appSettings = new DOPrintSettings("", "", 0, "/", "", 0, 0, 0, 0);


    private final boolean isScannerOpenTrue = true;

    private ActivityLabelPrintBinding binding;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private final boolean flag = false;
    private boolean ScannerOpen = true;
    private List<Masterdata> masterDataList;
    private int quantity = 1;
    private TextView productDesc, price;
    private boolean masterData = false;
    private String barcodeType;
    ImageView image;
    private MediaPlayer mediaPlayer;
    private Bitmap printableBitmap;


    private BroadcastReceiver barcodeDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_BARCODE_DATA.equals(intent.getAction())) {
                int version = intent.getIntExtra("version", 0);
                if (version >= 1) {
                    String aimId = intent.getStringExtra("aimId");
                    String charset = intent.getStringExtra("charset");
                    String codeId = intent.getStringExtra("codeId");
                    String data = intent.getStringExtra("data");
                    byte[] dataBytes = intent.getByteArrayExtra("dataBytes");
                    String timestamp = intent.getStringExtra("timestamp");

                    if (sharedPreferences.getString(SharedPref.TONE, "").equals("Tone 1")) {
                        if (mediaPlayer != null) {
                            mediaPlayer.release();
                        }
                        mediaPlayer = MediaPlayer.create(LabelPrintActivity.this, R.raw.tone_one);
                        mediaPlayer.start();

                    } else if (sharedPreferences.getString(SharedPref.TONE, "").equals("Tone 2")) {
                        if (mediaPlayer != null) {
                            mediaPlayer.release();
                        }
                        mediaPlayer = MediaPlayer.create(LabelPrintActivity.this, R.raw.tone_two);
                        mediaPlayer.start();

                    } else if (sharedPreferences.getString(SharedPref.TONE, "").equals("Tone 3")) {
                        if (mediaPlayer != null) {
                            mediaPlayer.release();
                        }
                        mediaPlayer = MediaPlayer.create(LabelPrintActivity.this, R.raw.tone_three);
                        mediaPlayer.start();
                    }

                    if (checkBarCode(codeId) == 1) {
                        binding.masterbarCodeFromSCET.setText(data);
                        binding.priceTV.setText(defineCodeName(codeId));
                    }

                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_label_print);

        init();

        getSharedPrederenceData();

        setDataIntoMaster();

        scannerTextWatch();

    }


    private void getSharedPrederenceData() {

        barcodeType = sharedPreferences.getString(SharedPref.PRINT_BARCODE_TYPE, "");

        if(checkBarCodeSettingStatus()){
            binding.masterbarCodeFromSCET.setEnabled(true);
        }else{
            binding.masterbarCodeFromSCET.setEnabled(false);
        }


    }

    private void scannerTextWatch() {
        binding.masterbarCodeFromSCET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                masterData = false;
                for (int i = 0; i < masterDataList.size(); i++) {
                    Log.d("TAG", "onTextChanged: " + masterDataList.get(i).getBarcode());

                    if (masterDataList.get(i).getBarcode().equals(binding.masterbarCodeFromSCET.getText().toString())) {
                        Log.d("TAG", "onEditorAction: exist");
                        binding.afterInputLL.setVisibility(View.VISIBLE);
                        binding.scanMAsterCodeLL.setVisibility(View.GONE);
                        binding.fabButton1.setVisibility(View.VISIBLE);
                        binding.quantityPrint.setVisibility(View.VISIBLE);
                        binding.quantityTV.setText(String.valueOf(quantity));
                        binding.barcodeET1.setText(masterDataList.get(i).getBarcode());
                        binding.underline2.setVisibility(View.VISIBLE);
                        binding.barcodeET1.requestFocus();
                        binding.descTV.setText(masterDataList.get(i).getDescription());
                        binding.priceTV.setText(masterDataList.get(i).getPrice());
                        binding.descriptionTitleTv.setText("Description");
                        binding.priceTitleTv.setText("Price");
                        getBarcode(masterDataList.get(i));
                        masterData = true;

                    } else {

                    }
                }
                if (!masterData) {
                    binding.afterInputLL.setVisibility(View.VISIBLE);
                    binding.scanMAsterCodeLL.setVisibility(View.GONE);
                    binding.fabButton1.setVisibility(View.VISIBLE);
                    binding.quantityPrint.setVisibility(View.VISIBLE);
                    binding.quantityTV.setText(String.valueOf(quantity));
                    binding.underline2.setVisibility(View.VISIBLE);
                    binding.barcodeET1.setText(binding.masterbarCodeFromSCET.getText().toString());
                    binding.masterbarCodeFromET.requestFocus();
                    binding.barcodeET1.setSelectAllOnFocus(true);
                    binding.barcodeET1.requestFocus();
                    binding.barcodeET1.setSelectAllOnFocus(true);
                    disableFocus();
                    hideKeyboard(LabelPrintActivity.this);
                    binding.descriptionTitleTv.setText("Barcode Type");
                    binding.priceTitleTv.setText("Digits");
                    binding.priceTV.setText(binding.masterbarCodeFromSCET.getText().toString().length() + "");
                    getBarcode(new Masterdata(binding.masterbarCodeFromSCET.getText().toString(), "", "", ""));
                }


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(barcodeDataReceiver, new IntentFilter(ACTION_BARCODE_DATA));
        claimScanner();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(barcodeDataReceiver);
        releaseScanner();

    }

    private void claimScanner() {
        Bundle properties = new Bundle();
        properties.putBoolean("DPR_DATA_INTENT", true);
        properties.putString("DPR_DATA_INTENT_ACTION", ACTION_BARCODE_DATA);
        sendBroadcast(new Intent(ACTION_CLAIM_SCANNER).setPackage("com.intermec.datacollectionservice")
                .putExtra(EXTRA_SCANNER, "dcs.scanner.imager").putExtra(EXTRA_PROFILE, "MyProfile1").putExtra(EXTRA_PROPERTIES, properties)
        );
    }

    private void releaseScanner() {
        sendBroadcast(new Intent(ACTION_RELEASE_SCANNER)
        );
    }


    public void openKeyboard(View view) {
        binding.scanBarcodeLL.setVisibility(View.GONE);
        binding.editbarcodeLL.setVisibility(View.VISIBLE);
        ScannerOpen = false;
        binding.masterbarCodeFromET.requestFocus();
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        checkDatainMasterList();
    }

    public void openScanner(View view) {
        binding.scanBarcodeLL.setVisibility(View.VISIBLE);
        binding.editbarcodeLL.setVisibility(View.GONE);
        ScannerOpen = true;
        disableFocus();
        hideKeyboard(this);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void disableFocus() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public void backLabel(View view) {
        onBackPressed();
        hideKeyboard(this);
    }

    private void setDataIntoMaster() {

        MasterExecutor.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                masterDataList = MasterData_DB.getInstance(getApplicationContext()).MasterdataDao().loadAllData();
                Log.d("TAG", "run: " + masterDataList.size());
            }
        });
    }


    private void init() {
        sharedPreferences = getSharedPreferences(SharedPref.SETTING_PREFERENCE, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        masterDataList = new ArrayList<>();
        binding.labelQnty.setVisibility(View.INVISIBLE);
        binding.imageViewbarcode.setVisibility(View.GONE);
        binding.afterInputLL.setVisibility(View.GONE);
        binding.fabButton1.setVisibility(View.GONE);
        productDesc = findViewById(R.id.productDescTV);
        price = findViewById(R.id.productPriceTV);
        image = findViewById(R.id.barcodeIV);
    }


    public void checkDatainMasterList() {
        if (ScannerOpen) {
            Log.d("TAG", "checkDatainMasterList: ");
        } else {
            Log.d("TAG", "checkDatainMasterList: ");
            inputMasterTypeBarcode();
        }
    }


    private void inputMasterTypeBarcode() {

        binding.masterbarCodeFromET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.d("TAG", "onEditorAction: " + binding.masterbarCodeFromET.getText().toString());
                    masterData = false;
                    for (int i = 0; i < masterDataList.size(); i++) {
                        Log.d("TAG", "onTextChanged: " + masterDataList.get(i).getBarcode());

                        if (masterDataList.get(i).getBarcode().equals(binding.masterbarCodeFromET.getText().toString())) {
                            Log.d("TAG", "onEditorAction: exist");
                            binding.afterInputLL.setVisibility(View.VISIBLE);
                            binding.scanMAsterCodeLL.setVisibility(View.GONE);
                            binding.fabButton1.setVisibility(View.VISIBLE);
                            binding.quantityPrint.setVisibility(View.VISIBLE);
                            binding.quantityTV.setText(String.valueOf(quantity));
                            binding.barcodeET1.setText(masterDataList.get(i).getBarcode());
                            binding.underline2.setVisibility(View.VISIBLE);
                            binding.barcodeET1.requestFocus();
                            binding.descTV.setText(masterDataList.get(i).getDescription());
                            binding.priceTV.setText(masterDataList.get(i).getPrice());
                            binding.descriptionTitleTv.setText("Description");
                            binding.priceTitleTv.setText("Price");
                            getBarcode(masterDataList.get(i));
                            masterData = true;

                        } else {

                        }
                    }
                    if (!masterData) {
                        binding.afterInputLL.setVisibility(View.VISIBLE);
                        binding.scanMAsterCodeLL.setVisibility(View.GONE);
                        binding.fabButton1.setVisibility(View.VISIBLE);
                        binding.quantityPrint.setVisibility(View.VISIBLE);
                        binding.quantityTV.setText(String.valueOf(quantity));
                        binding.underline2.setVisibility(View.VISIBLE);
                        binding.barcodeET1.setText(binding.masterbarCodeFromET.getText().toString());
                        binding.masterbarCodeFromET.requestFocus();
                        binding.barcodeET1.setSelectAllOnFocus(true);
                        binding.barcodeET1.requestFocus();
                        binding.barcodeET1.setSelectAllOnFocus(true);
                        disableFocus();
                        hideKeyboard(LabelPrintActivity.this);
                        binding.descriptionTitleTv.setText("Barcode Type");
                        binding.priceTitleTv.setText("Digits");
                        binding.priceTV.setText(binding.masterbarCodeFromET.getText().toString().length() + "");
                        getBarcode(new Masterdata(binding.masterbarCodeFromET.getText().toString(), "", "", ""));
                    }

                    handled = true;
                }
                return handled;
            }
        });

    }

    public void getBarcode(Masterdata data) {
        Bitmap bitmap = null;


        try {

            if (barcodeType.equals("Code 128")) {
                bitmap = encodeAsBitmap(data.getBarcode(), BarcodeFormat.CODE_128, 600, 300);
            } else if (barcodeType.equals("2D Datamatrix")) {
                bitmap = encodeAsBitmap(data.getBarcode(), BarcodeFormat.QR_CODE, 400, 400);
            } else {
                bitmap = encodeAsBitmap(data.getBarcode(), BarcodeFormat.CODE_128, 600, 300);
            }


            printableBitmap = bitmap;
            //binding.imageViewbarcode.setImageBitmap(bitmap);
            binding.labelQnty.setVisibility(View.VISIBLE);
            finalLayout(bitmap, data);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }


    public void decrementQuantity(View view) {
        if (quantity != 1) {
            quantity--;
            binding.quantityTV.setText(String.valueOf(quantity));
        }
    }

    public void incrementQuantity(View view) {
        if (quantity >= 1 && quantity != 10) {
            quantity++;
            binding.quantityTV.setText(String.valueOf(quantity));
        }
    }

    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }


    public void finalLayout(Bitmap barcodeImage, Masterdata data) {
        productDesc.setText(data.getDescription());
        if (data.getPrice().equals("") || data.getPrice().equals("null")) {
            price.setText("");
        } else {
            price.setText(data.getPrice());
        }
        image.setImageBitmap(barcodeImage);
        Bitmap bitmap = Bitmap.createBitmap(binding.labelQnty.getWidth() + 100, binding.labelQnty.getHeight() + 50,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        binding.labelQnty.draw(canvas);
        binding.imageViewbarcode.setVisibility(View.VISIBLE);
        binding.imageViewbarcode.setImageBitmap(bitmap);
        binding.labelQnty.setVisibility(View.INVISIBLE);

    }


    private String defineCodeName(String codeId) {
        String codeName;
        switch (codeId) {
            case ".":
                codeName = "DOTCODE";
                break;
            case "1":
                codeName = "CODE1";
                break;
            case ";":
                codeName = "MERGED_COUPON";
                break;
            case "<":
                codeName = "CODE32";
                break;
            case ">":
                codeName = "LABELCODE_IV";
                break;
            case "=":
                codeName = "TRIOPTIC";
                break;
            case "'?'":
                codeName = "KOREA_POST";
                break;
            case ",":
                codeName = "INFOMAIL";
                break;
            case "[":
                codeName = "SWEEDISH_POST";
                break;
            case "|":
                codeName = "RM_MAILMARK";
                break;
            case "'":
                codeName = "EAN13_ISBN";
                break;
            case "]":
                codeName = "BRAZIL_POS";
                break;
            case "A":
                codeName = "AUS_POST";
                break;
            case "B":
                codeName = "BRITISH_POST";
                break;
            case "C":
                codeName = "CANADIAN_POST";
                break;
            case "D":
                codeName = "EAN8";
                break;
            case "E":
                codeName = "UPCE";
                break;
            case "G'":
                codeName = "BC412";
                break;
            case "H":
                codeName = "HAN_XIN_CODE";
                break;
            case "I":
                codeName = "GS1_128";
                break;
            case "J":
                codeName = "JAPAN_POST";
                break;
            case "K":
                codeName = "KIX_CODE";
                break;
            case "L":
                codeName = "PLANET_CODE";
                break;
            case "M":
                codeName = "INTELLIGENT_MAIL";
                break;
            case "N":
                codeName = "ID_TAGS";
                break;
            case "O":
                codeName = "OCR";
                break;
            case "P":
                codeName = "POSTNET";
                break;
            case "Q":
                codeName = "HK25";
                break;
            case "R":
                codeName = "MICROPDF";
                break;
            case "S":
                codeName = "SECURE_CODE";
                break;
            case "T":
                codeName = "TLC39";
                break;
            case "U":
                codeName = "ULTRACODE";
                break;
            case "V":
                codeName = "CODABLOCK_A";
                break;
            case "W":
                codeName = "POSICODE";
                break;
            case "X":
                codeName = "GRID_MATRIX";
                break;
            case "Y":
                codeName = "NEC25";
                break;
            case "Z":
                codeName = "MESA";
                break;
            case "a":
                codeName = "CODABAR";
                break;
            case "b":
                codeName = "CODE39";
                break;
            case "c":
                codeName = "UPCA";
                break;
            case "d":
                codeName = "EAN13";
                break;
            case "e":
                codeName = "I25";
                break;
            case "f":
                codeName = "S25 (2BAR and 3BAR)";
                break;
            case "g":
                codeName = "MSI";
                break;
            case "h":
                codeName = "CODE11";
                break;
            case "i":
                codeName = "CODE93";
                break;
            case "j":
                codeName = "CODE128";
                break;
            case "k":
                codeName = "UNUSED";
                break;
            case "l":
                codeName = "CODE49";
                break;
            case "m":
                codeName = "M25";
                break;
            case "n":
                codeName = "PLESSEY";
                break;
            case "o":
                codeName = "CODE16K";
                break;
            case "p":
                codeName = "CHANNELCODE";
                break;
            case "q":
                codeName = "CODABLOCK_F";
                break;
            case "r":
                codeName = "PDF417";
                break;
            case "s":
                codeName = "QRCODE";
                break;
            case "-":
                codeName = "MICROQR_ALT";
                break;
            case "t":
                codeName = "TELEPEN";
                break;
            case "u":
                codeName = "CODEZ";
                break;
            case "v":
                codeName = "VERICODE";
                break;
            case "w":
                codeName = "DATAMATRIX";
                break;
            case "x":
                codeName = "MAXICODE";
                break;
            case "y":
                codeName = "RSS";
                break;
            case "{":
                codeName = "GS1_DATABAR";
                break;
            case "}":
                codeName = "GS1_DATABAR_EXP";
                break;
            case "z":
                codeName = "AZTEC_CODE";
                break;
            default:
                codeName = "";
        }
        return codeName;
    }

    private int checkBarCode(String codeId) {
        int value = 0;

        if (codeId.equals("b") && sharedPreferences.getBoolean(SharedPref.CODE_39, false)) {
            value = 1;
        } else if (codeId.equals("j") && sharedPreferences.getBoolean(SharedPref.CODE_128, false)) {
            value = 1;
        } else if (codeId.equals("d") && sharedPreferences.getBoolean(SharedPref.EAN_13, false)) {
            value = 1;
        } else if (codeId.equals("w") && sharedPreferences.getBoolean(SharedPref.DATA_MATRIX, false)) {
            value = 1;
        } else if (codeId.equals("s") && sharedPreferences.getBoolean(SharedPref.QR_CODE, false)) {
            value = 1;
        } else if (codeId.equals("e") && sharedPreferences.getBoolean(SharedPref.CODE_TWO_BY_FIVE, false)) {
            value = 1;
        } else {
            value = 0;
        }

        return value;
    }

    private boolean checkBarCodeSettingStatus() {
        boolean value = false;

        if (sharedPreferences.getBoolean(SharedPref.CODE_39, false)) {
            value = true;
        } else if (sharedPreferences.getBoolean(SharedPref.CODE_128, false)) {
            value = true;
        } else if (sharedPreferences.getBoolean(SharedPref.EAN_13, false)) {
            value = true;
        } else if (sharedPreferences.getBoolean(SharedPref.DATA_MATRIX, false)) {
            value = true;
        } else if (sharedPreferences.getBoolean(SharedPref.QR_CODE, false)) {
            value = true;
        } else if (sharedPreferences.getBoolean(SharedPref.CODE_TWO_BY_FIVE, false)) {
            value = true;
        }  else {
            value = false;
        }

        return value;

    }

    public void OpenConnectionSetting(View view) {

        Intent connSettingsIntent = new Intent(LabelPrintActivity.this, LabelPrintingConnectionSettingActivity.class);
        String connectionType = "Bluetooth";
        connSettingsIntent.putExtra(CONNECTION_MODE_KEY, connectionType);

        connSettingsIntent.putExtra(PRINTER_IPADDRESS_KEY, m_printerIP);
        connSettingsIntent.putExtra(PRINTER_TCPIPPORT_KEY, m_printerPort);
        connSettingsIntent.putExtra(BLUETOOTH_DEVICE_ADDR_KEY, m_printerMAC);

        startActivityForResult(connSettingsIntent, CONFIG_CONNECTION_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            //get results from connection settings activity
            case CONFIG_CONNECTION_REQUEST: {
                if (resultCode == RESULT_OK) {
                    // get the bundle data from the TCP/IP Config Intent.
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        String btAddressString = extras.getString(BLUETOOTH_DEVICE_ADDR_KEY);
                        if (btAddressString != null)
                            m_printerMAC = btAddressString.toUpperCase(Locale.US);
                        if (!m_printerMAC.matches("[0-9A-fa-f:]{17}")) {
                            m_printerMAC = formatBluetoothAddress(m_printerMAC);
                        }
                        Toast.makeText(this, "" + m_printerMAC, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }
        }
    }

    public void fabButtonClicktoPrint(View view) {

        if(m_printerMAC != null && m_printerMAC.length() > 0) {

            try {
                printData = new byte[]{0};
                docLP = new DocumentLP("!");
                docExPCL_LP = new DocumentExPCL_LP(3);
                docExPCL_PP = new DocumentExPCL_PP(DocumentExPCL_PP.PaperWidth.PaperWidth_384);
                //if we are printing
                if (printableBitmap != null) {
                    docLP.writeImage(printableBitmap, m_printHeadWidth);

                    for (int i = 0; i < Integer.parseInt(binding.quantityTV.getText().toString()); i++) {
                        printData = docLP.getDocumentData();
                    }
                }
                //=====================Start Connection Thread=======================================//
                new Thread((Runnable) LabelPrintActivity.this, "PrintingTask").start();
            } catch (Exception e) {
                // Application error message box
                e.printStackTrace();
                AlertDialog.Builder builder = new AlertDialog.Builder(LabelPrintActivity.this);
                builder.setTitle("Application Error")
                        .setMessage(e.getMessage())
                        .setCancelable(false)
                        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }else{
            Toast.makeText(this, "Connect your device", Toast.LENGTH_SHORT).show();
        }

    }

    public void EnableControls(final boolean value) {
        m_handler.post(new Runnable() {

            @Override
            public void run() {
                binding.fabButton1.setEnabled(value);
            }
        });
    }


    public String formatBluetoothAddress(String bluetoothAddr) {
        //Format MAC address string
        StringBuilder formattedBTAddress = new StringBuilder(bluetoothAddr);
        for (int bluetoothAddrPosition = 2; bluetoothAddrPosition <= formattedBTAddress.length() - 2; bluetoothAddrPosition += 3)
            formattedBTAddress.insert(bluetoothAddrPosition, ":");
        return formattedBTAddress.toString();
    }

    @Override
    public void run() {
        //Connection
        try {
            EnableControls(false);
            //Reset connection object
            conn = null;
            //====FOR BLUETOOTH CONNECTIONS========//

            Looper.prepare();
            conn = Connection_Bluetooth.createClient(m_printerMAC, false);

            Log.d("sajib", "runConn: "+conn.toString());
            Toast.makeText(this, "Establishing connection..", Toast.LENGTH_SHORT).show();
            //Open bluetooth socket
            if (!conn.getIsOpen()) {
                conn.open();
                Log.d("sajib", "runConnOPen: "+conn.toString());
            }

            Toast.makeText(this, "Sending data to printer..", Toast.LENGTH_SHORT).show();
            //Sends data to printer

            int bytesWritten = 0;
            int bytesToWrite = 1024;
            int totalBytes = printData.length;
            int remainingBytes = totalBytes;
            while (bytesWritten < totalBytes) {
                if (remainingBytes < bytesToWrite)
                    bytesToWrite = remainingBytes;

                //Send data, 1024 bytes at a time until all data sent
                conn.write(printData, bytesWritten, bytesToWrite);
                bytesWritten += bytesToWrite;
                remainingBytes = remainingBytes - bytesToWrite;
                Thread.sleep(100);
            }

            //signals to close connection
            conn.close();
            Toast.makeText(this, "Print success", Toast.LENGTH_SHORT).show();
            EnableControls(true);


        } catch (Exception e) {
            //signals to close connection
            if (conn != null)
                conn.close();
            Toast.makeText(this, "Client device socket might closed or timeout", Toast.LENGTH_SHORT).show();
            Log.d("sajib", "runConnClose: "+conn.toString());
            e.printStackTrace();
            // Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.d("sajib", "run: "+e.getMessage() );
            EnableControls(true);
        }
    }
}